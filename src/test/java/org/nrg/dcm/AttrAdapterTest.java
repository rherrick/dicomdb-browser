/**
 * Copyright (c) 2006-2009 Washington University
 */
package org.nrg.dcm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dcm4che2.data.Tag;
import org.junit.Test;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrDef;
import org.nrg.attr.ExtAttrException;
import org.nrg.attr.ExtAttrValue;

import com.google.common.collect.ListMultimap;


@SuppressWarnings("unchecked")
public class AttrAdapterTest {
  // Uses sample data available here:
  // http://nrg.wustl.edu/projects/DICOM/sample1.zip
  final static private File fsdir = new File(System.getProperty("sample.data.dir"));
  final static String[] filenames = {
    "1.MR.head_DHead.4.3.20061214.091206.156000.0506717986.dcm.gz",
    "1.MR.head_DHead.4.11.20061214.091206.156000.8007818018.dcm.gz",
    "1.MR.head_DHead.4.12.20061214.091206.156000.6027118028.dcm.gz"
  };
  final static FileSet fs;
  static {
    try {
      fs = new FileSet(fsdir);
    } catch (IOException e) {
      fail("Unable to create FileSet: " + e.getMessage());
      throw new RuntimeException("fail failed?");
    } catch (SQLException e) {
      fail("Unable to create FileSet: " + e.getMessage());
      throw new RuntimeException("fail failed?");
    }
    if (fs == null) {
      fail("Unable to create FileSet");
    }
  }

  @Test
  public final void testAttrAdapter() {
    AttrAdapter aa = new AttrAdapter(fs);
    assertNotNull(aa);
  }

  @Test
  public final void testAddSet() {
    AttrDefSet attrSet = new AttrDefSet();
    attrSet.add("foo:bar", 0);
    attrSet.add("bar:baz", 1);
    AttrAdapter aa = new AttrAdapter(fs);
    aa.add(attrSet);
    try {
      aa.add(new TestAttrDef.Text("foo:bar", 1));
      fail("Attempted attribute redefinition must result in assertion failure");
    } catch (Throwable e) {
      assertTrue(e instanceof IllegalArgumentException);
    }
  }

  @Test
  public final void testAddAttr() {
    AttrAdapter aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Text("just date", Tag.StudyDate));
  }

  @Test
  public final void testRemoveString() {
    AttrAdapter aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Abstract("date-and-time", Tag.StudyDate, Tag.StudyTime) {
      @Override
      public String convertText(Map<Integer,String> attrs) {
        String dt = attrs.get(Tag.StudyDate) + " at " + attrs.get(Tag.StudyTime);
        return dt;
      }
    });
    aa.add(new TestAttrDef.Text("just-date", Tag.StudyDate));
    assertEquals(1, aa.remove("just-date"));

    aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Abstract("date-and-time", Tag.StudyDate, Tag.StudyTime) {
      @Override
      public String convertText(Map<Integer,String> attrs) {
        String dt = attrs.get(Tag.StudyDate) + " at " + attrs.get(Tag.StudyTime);
        return dt;
      }
    });
    aa.add(new TestAttrDef.Text("just-date", Tag.StudyDate));
    assertEquals(1, aa.remove("date-and-time"));

    aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Abstract("date-and-time", Tag.StudyDate, Tag.StudyTime) {
      @Override
      public String convertText(Map<Integer,String> attrs) {
        String dt = attrs.get(Tag.StudyDate) + " at " + attrs.get(Tag.StudyTime);
        return dt;
      }
    });
    aa.add(new TestAttrDef.Text("just-date", Tag.StudyDate));
    assertEquals(0, aa.remove("foo"));
    assertEquals(2, aa.remove("date-and-time", "just-date"));
    assertEquals(0, aa.remove("just-date"));

    aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Abstract("date-and-time", Tag.StudyDate, Tag.StudyTime) {
      @Override
      public String convertText(Map<Integer,String> attrs) {
        String dt = attrs.get(Tag.StudyDate) + " at " + attrs.get(Tag.StudyTime);
        return dt;
      }
    });
    aa.add(new TestAttrDef.Text("just-date", Tag.StudyDate));
    assertEquals(1, aa.remove("date-and-time", "foo"));
  }

  @Test
  public final void testRemoveInt() {
    AttrAdapter aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Abstract("date-and-time", Tag.StudyDate, Tag.StudyTime) {
      @Override
      public String convertText(Map<Integer,String> attrs) {
        String dt = attrs.get(Tag.StudyDate) + " at " + attrs.get(Tag.StudyTime);
        return dt;
      }
    });
    aa.add(new TestAttrDef.Text("just-date", Tag.StudyDate));
    assertEquals(1, aa.remove(Tag.StudyTime));
    assertEquals(1, aa.remove(Tag.StudyDate));
    assertEquals(0, aa.remove(Tag.StudyTime));

    aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Abstract("date-and-time", Tag.StudyDate, Tag.StudyTime) {
      @Override
      public String convertText(Map<Integer,String> attrs) {
        String dt = attrs.get(Tag.StudyDate) + " at " + attrs.get(Tag.StudyTime);
        return dt;
      }
    });
    aa.add(new TestAttrDef.Text("just-date", Tag.StudyDate));
    assertEquals(2, aa.remove(Tag.StudyDate));
    assertEquals(0, aa.remove(Tag.StudyTime));

    aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Abstract("date-and-time", Tag.StudyDate, Tag.StudyTime) {
      @Override
      public String convertText(Map<Integer,String> attrs) {
        String dt = attrs.get(Tag.StudyDate) + " at " + attrs.get(Tag.StudyTime);
        return dt;
      }
    });
    aa.add(new TestAttrDef.Text("just-date", Tag.StudyDate));
    assertEquals(0, aa.remove(Tag.PatientID));
    assertEquals(2, aa.remove(Tag.StudyTime, Tag.StudyDate));
    assertEquals(0, aa.remove(Tag.StudyDate));

    aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Abstract("date-and-time", Tag.StudyDate, Tag.StudyTime) {
      @Override
      public String convertText(Map<Integer,String> attrs) {
        String dt = attrs.get(Tag.StudyDate) + " at " + attrs.get(Tag.StudyTime);
        return dt;
      }
    });
    aa.add(new TestAttrDef.Text("just-date", Tag.StudyDate));
    assertEquals(1, aa.remove(Tag.PatientID, Tag.StudyTime));
  }


  @Test
  public final void testGetValuesGiven() {
    final Map<ExtAttrDef<Integer,String>,Exception> failures = new HashMap<ExtAttrDef<Integer,String>,Exception>();
    final AttrAdapter aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Abstract("voxelRes", Tag.PixelSpacing, Tag.SliceThickness) {
      @Override
      public String convertText(Map<Integer,String> attrs) {
        String vrxy = attrs.get(Tag.PixelSpacing).replaceAll("\\\\", ", ");
        return vrxy + ", " + attrs.get(Tag.SliceThickness);
      }
    });
    aa.add(new TestAttrDef.Empty("empty"));
    Map<Integer,String> scanSpec = new HashMap<Integer,String>();
    scanSpec.put(Tag.SeriesNumber, "4");
    List<ExtAttrValue> vals = null;
    try {
      vals = aa.getValuesGiven(scanSpec, failures);
      assertTrue(failures.isEmpty());
    } catch (ExtAttrException e) {
      fail(e.getMessage());
    }

    assertNotNull(vals);
    assert vals != null;
    assertEquals(2, vals.size());
    ExtAttrValue val = vals.get(0);
    assertEquals("voxelRes", val.getName());
    assertEquals("1, 1, 1", val.getText());
    assertEquals("empty", vals.get(1).getName());
  }

  @Test
  public final void testGetValues() {
    final Map<ExtAttrDef<Integer,String>,Exception> failures = new HashMap<ExtAttrDef<Integer,String>,Exception>();
    final AttrAdapter aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Abstract("date-and-time", Tag.StudyDate,Tag.StudyTime) {
      @Override
      public String convertText(Map<Integer,String> attrs) {
        String dt = attrs.get(Tag.StudyDate) + " at " + attrs.get(Tag.StudyTime);
        return dt;
      }
    });
    aa.add(new TestAttrDef.Empty("empty"));
    List<ExtAttrValue> vals = null;
    try {
      vals = aa.getValues(failures);
      assertTrue(failures.isEmpty());
    } catch (ExtAttrException e) {
      fail(e.getMessage());
    }

    assertNotNull(vals);
    assert vals != null;
    assertEquals(2, vals.size());
    ExtAttrValue val = vals.get(0);
    assertEquals("date-and-time", val.getName());
    assertEquals("20061214 at 091206.156000", val.getText());
    val = vals.get(1);
    assertEquals("empty", val.getName());
  }

  @Test
  public final void testGetValuesForFiles() throws Exception {
    final AttrDefSet s = new AttrDefSet();
    s.add("scanner", Tag.StationName);
    s.add("operator", Tag.OperatorsName);
    s.add("sessionPatientName", Tag.PatientName);
    s.add(new TestAttrDef.Empty("empty"));

    final AttrAdapter aa = new AttrAdapter(fs);
    aa.add(s);

    final File onefile;
    try {
      onefile = new File(fsdir, filenames[0]).getCanonicalFile();
    } catch (IOException e) {
      fail("unable to get canonical file: " + e.getMessage());
      return;
    }

    ListMultimap<File,ExtAttrValue> vals = aa.getValuesForFiles(onefile);
    assertNotNull(vals);

    assertEquals(1, vals.keySet().size());
    assertTrue(vals.containsKey(onefile));
    assertEquals("scanner", vals.get(onefile).get(0).getName());
    assertEquals("operator", vals.get(onefile).get(1).getName());
    assertEquals("sessionPatientName", vals.get(onefile).get(2).getName());
    assertEquals("empty", vals.get(onefile).get(3).getName());

    File[] files = new File[filenames.length];
    for (int i = 0; i < files.length; i++)
      try {
        files[i] = new File(fsdir, filenames[i]).getCanonicalFile();
      } catch (IOException e) {
        fail("Unable to get canonical file: " + e.getMessage());
      }

      vals = aa.getValuesForFiles(files);
      assertNotNull(vals);
      assertEquals(3, vals.keySet().size());
  }


  @Test
  public final void testGetMultipleValuesGiven() {
    final Map<ExtAttrDef<Integer,String>,Exception> failures = new HashMap<ExtAttrDef<Integer,String>,Exception>();
    final AttrAdapter aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Text("series number", Tag.SeriesNumber));
    aa.add(new TestAttrDef.Empty("empty"));	// this causes problems
    Map<Integer,String> scanSpec = new HashMap<Integer,String>();
    scanSpec.put(Tag.StudyDate, "20061214");
    final List<Set<ExtAttrValue>> vals;
    try {
      vals = aa.getMultipleValuesGiven(scanSpec, failures);
    } catch (ConversionFailureException e) {
      fail(e.getMessage() + " for " + e.getAttr());
      return;
    } catch (ExtAttrException e) {
      fail(e.getMessage());
      return;
    }
    assertNotNull(vals);
    assert vals != null;

    assertEquals(2, vals.size());
    assertEquals(3, vals.get(0).size());
    assertEquals(1, vals.get(1).size());        // only one value (null) of empty attribute
  }

  @Test
  public final void testGetMultipleValues() {
    final Map<ExtAttrDef<Integer,String>,Exception> failures = new HashMap<ExtAttrDef<Integer,String>,Exception>();
    final AttrAdapter aa = new AttrAdapter(fs);
    aa.add(new TestAttrDef.Text("series number", Tag.SeriesNumber));
    aa.add(new TestAttrDef.Empty("empty"));
    List<Set<ExtAttrValue>> vals = null;
    try {
      vals = aa.getMultipleValuesGiven(new HashMap<Integer,String>(), failures);
    } catch (ExtAttrException e) {
      fail(e.getMessage());
    }
    assertNotNull(vals);
    assert vals != null;
    assertEquals(2, vals.size());
    assertEquals(3, vals.get(0).size());
    assertEquals(1, vals.get(1).size());
  }
}
