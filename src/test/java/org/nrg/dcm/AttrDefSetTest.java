/**
 * Copyright (c) 2006-2009 Washington University
 */
package org.nrg.dcm;

import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

import static org.junit.Assert.*;

import org.junit.Test;

import org.nrg.dcm.TestAttrDef;
import org.nrg.attr.ExtAttrDef;

@SuppressWarnings("unchecked")
public class AttrDefSetTest {
  @Test
  public final void testAddExtAttrDef() {
    AttrDefSet a = new AttrDefSet();
    int count = 0;
    for (@SuppressWarnings("unused") ExtAttrDef<Integer,String> ea : a)
      count++;
    assertEquals(0, count);
    assertEquals(0, a.getNativeAttrs().size());
    
    a.add(new TestAttrDef.Text("foo", 1));
    assertEquals(1, a.getNativeAttrs().size());
    count = 0;
    for (ExtAttrDef<Integer,String> ea : a)
      assertEquals("foo", ea.getName());
    
    a.add(new TestAttrDef.Text("bar", 16));
    a.add(new TestAttrDef.Text("baz", 8));
    
    Iterator<ExtAttrDef<Integer,String>> eai = a.iterator();
    assertTrue(eai.hasNext());
    assertEquals("foo", eai.next().getName());
    assertEquals("bar", eai.next().getName());
    assertEquals("baz", eai.next().getName());
    
  }

  @Test
  public final void testAddStringInt() {
    AttrDefSet a = new AttrDefSet();
    int count = 0;
    for (@SuppressWarnings("unused") ExtAttrDef<Integer,String> ea : a)
      count++;
    assertEquals(0, count);
    assertEquals(0, a.getNativeAttrs().size());
    
    a.add("foo", 1);
    assertEquals(1, a.getNativeAttrs().size());
    count = 0;
    for (ExtAttrDef<Integer,String> ea : a)
      assertEquals("foo", ea.getName());
  }

  @Test
  public final void testAddReadableAttrDefSetArray() {
    AttrDefSet a1 = new AttrDefSet();
    AttrDefSet a2 = new AttrDefSet();
    a2.add("foo", 1);
    a2.add("bar", 2);
    a2.add("baz", 3);
    
    a1.add(a2);
    assertEquals(3, a1.getNativeAttrs().size());
    
    AttrDefSet a3 = new AttrDefSet();
    a3.add("ack", 4);
    a3.add(a2);
    assertEquals(4, a3.getNativeAttrs().size());
  }

  @Test
  public final void testRemoveString() {
    AttrDefSet a1 = new AttrDefSet();
    a1.add("foo", 1);
    a1.add("bar", 2);
    a1.add("baz", 3);
    assertEquals(3, a1.getNativeAttrs().size());
    assertEquals(1, a1.remove("bar"));
    for (ExtAttrDef<Integer,String> ea : a1)
      assertFalse(ea.getName().equals("bar"));
    assertEquals(0, a1.remove("bar"));
  }

  @Test
  public final void testRemoveInt() {
    AttrDefSet a1 = new AttrDefSet();
    a1.add("foo", 1);
    a1.add("bar", 2);
    a1.add("baz", 3);
    a1.add("yak", 2);
    assertEquals(3, a1.getNativeAttrs().size());
    a1.remove(2);
    assertEquals(2, a1.getNativeAttrs().size());
    for (ExtAttrDef<Integer,String> ea : a1) {
      assertFalse(ea.getAttrs().contains(2));
      assertFalse(ea.getName().equals("bar"));
      assertFalse(ea.getName().equals("yak"));
    }
  }

  @Test
  public final void testIterator() {
    String[] namea = {"foo", "bar", "baz", "yak"};
    assert namea.length > 0;
    
    Set<String> names = new HashSet<String>(Arrays.asList(namea));
    assert namea.length == names.size();
    
    AttrDefSet a = new AttrDefSet();
    for (int i = 0; i < namea.length; i++)
      a.add(namea[i], i);
    assertEquals(namea.length, a.getNativeAttrs().size());

    Iterator<ExtAttrDef<Integer,String>> eai = a.iterator();
    assertEquals("foo", eai.next().getName());
    assertEquals("bar", eai.next().getName());
    assertEquals("baz", eai.next().getName());
    assertEquals("yak", eai.next().getName());
   
    for (ExtAttrDef<Integer,String> ea : a) {
      assertTrue(names.contains(ea.getName()));
      names.remove(ea.getName());
    }
    assertEquals(0, names.size());
  }

  @Test
  public final void testGetExtAttrDef() {
    String[] namea = {"foo", "bar", "baz", "yak"};
    assert namea.length > 0;

    AttrDefSet a = new AttrDefSet();
    for (int i = 0; i < namea.length; i++)
      a.add(namea[i], i);
    assertEquals(namea.length, a.getNativeAttrs().size());

    for (int i = 0; i < namea.length; i++) {
      ExtAttrDef<Integer,String> ea = a.getExtAttrDef(namea[i]);
      assertNotNull(ea);
      assertEquals(namea[i], ea.getName());
    }
  }

  @Test
  public final void testgetNativeAttrs() {
    String[] namea = {"foo", "bar", "baz", "yak"};
    assert namea.length > 0;

    AttrDefSet a = new AttrDefSet();
    for (int i = 0; i < namea.length; i++)
      a.add(namea[i], i);
    assertEquals(namea.length, a.getNativeAttrs().size());

    for (int i = 0; i < namea.length; i++) {
      assertTrue(a.getNativeAttrs().contains(i));
    }
  }

}
