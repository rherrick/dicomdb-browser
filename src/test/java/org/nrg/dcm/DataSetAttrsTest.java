/**
 * Copyright (c) 2006-2010 Washington University
 */
package org.nrg.dcm;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.DicomOutputStream;

import org.junit.Test;

import org.nrg.attr.ConversionFailureException;

public class DataSetAttrsTest {
	// Tests based on the sample1 dataset, with each file gzipped
	private static final File dataDir = new File(System.getProperty("sample.data.dir"));
	private static final File file = new File(dataDir, "1.MR.head_DHead.4.1.20061214.091206.156000.1632817982.dcm.gz");
	private static final File headless;
	static {
		try {
			headless = File.createTempFile("headless", ".dcm");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	final private String sequenceName = "*tfl3d1_ns";

	private static final String GZIP_SUFFIX = ".gz";

	private static final DicomObject readPart10File(final File f) throws IOException {
		InputStream fin = new FileInputStream(file);
		try {
			if (file.getName().endsWith(GZIP_SUFFIX)) {
				fin = new GZIPInputStream(fin);
			}
			final BufferedInputStream bin = new BufferedInputStream(fin);
			try {
				final DicomInputStream in = new DicomInputStream(bin);
				try {
					final DicomObject o = in.readDicomObject();
					if (o.contains(Tag.FileMetaInformationVersion)) {
						// looks part 10 compliant; assume it's valid
						return o;
					} else {
						throw new IOException(file.getPath()
								+ " is not a valid DICOM file: no part 10 header");
					}
				} finally {
					try { in.close(); } catch (IOException ignore) {}
				}
			} finally {
				try { bin.close(); } catch (IOException ignore) {}
			}
		} finally {
			try { fin.close(); } catch (IOException ignore) {}
		}
	}


	private static final void makeFMIlessCopy(final File dest, final File source)
	throws IOException {
		final DicomObject o = readPart10File(source);
		final DicomOutputStream dos = new DicomOutputStream(dest);
		try {
			dos.writeDataset(o, o.getString(Tag.TransferSyntaxUID));
		} finally {
			dos.close();
		}
	}


	@Test
	public final void testDataSetAttrsFileInt() throws IOException {
		try {
			final DataSetAttrs dsa = new DataSetAttrs(file, Tag.SequenceName);
			assertNotNull(dsa);

			makeFMIlessCopy(headless, file);
			final DataSetAttrs hdsa = new DataSetAttrs(headless, Tag.SequenceName);
			assertNotNull(hdsa);
		} finally {
			headless.delete();
		}
	}

	@Test
	public final void testDataSetAttrsFileSetOfInteger() throws IOException {
		Set<Integer> attrs = new HashSet<Integer>();

		// do it once with an empty attribute set...
		// empty attribute set is no longer allowed
		//    try {
		//      DataSetAttrs dsa = new DataSetAttrs(file, attrs);
		//      assertNotNull(dsa);
		//    } catch (IOException e) {
		//      fail("DataSetAttrs() generated an IOException: " + e);
		//    }

		attrs.add(Tag.SequenceName);
		attrs.add(Tag.AcquisitionDate);
		attrs.add(Tag.AcquisitionTime);
		attrs.add(Tag.OperatorsName);

		// and again with a nonempty set
		final DataSetAttrs dsa = new DataSetAttrs(file, attrs);
		assertNotNull(dsa);
	}

	@Test
	public final void testDataSetAttrsFile() throws IOException {
		DataSetAttrs dsa = new DataSetAttrs(file);
		assertNotNull(dsa);
	}

	@Test
	public final void testGet() throws IOException,ConversionFailureException {
		Set<Integer> attrs = new HashSet<Integer>();
		attrs.add(Tag.SequenceName);

		// test some values without conversions,
		// and at least one value of each type requiring conversion
		final DataSetAttrs dsa = new DataSetAttrs(file, attrs);
		assertNotNull(dsa);
		assertEquals(sequenceName, dsa.get(Tag.SequenceName));
	}

	@Test
	public void testConvert() throws ConversionFailureException {
		final DicomObject o = new BasicDicomObject();
		o.putInt(Tag.DataElementsSigned, VR.AT, 0x00010001);
		o.putInts(Tag.FailureAttributes, VR.AT, new int[]{0x00020001,0x00010003});
		assertEquals("(0001,0001)", DataSetAttrs.convert(o, Tag.DataElementsSigned));
		assertEquals("(0002,0001)\\(0001,0003)", DataSetAttrs.convert(o, Tag.FailureAttributes));
	}

	@Test
	public void testProblemVRs() throws IOException,ConversionFailureException {
		final DataSetAttrs dsa = new DataSetAttrs(file, Tag.PixelData - 1);
		assertNotNull(dsa);
		assertEquals(sequenceName, dsa.get(Tag.SequenceName));
		assertEquals("IMAGE NUM 4", dsa.get(0x00291008));    // Siemens-specific, UN VR
		//      assertEquals("(sequence)", dsa.get(Tag.IconImageSequence)); // SQ
		// Problem VR AT is tested in testConvert()
	}

	@Test
	public void testIterator() throws IOException {
		final DataSetAttrs dsa = new DataSetAttrs(file, Tag.PixelData - 1);
		assertNotNull(dsa);
		final Iterator<Integer> tagi = dsa.iterator();
		assertNotNull(tagi);
		assertTrue(tagi.hasNext());
		assertEquals(Integer.valueOf(Tag.SpecificCharacterSet), tagi.next());
		assertEquals(Integer.valueOf(Tag.ImageType), tagi.next());
		assertEquals(Integer.valueOf(Tag.InstanceCreationDate), tagi.next());
		assertEquals(Integer.valueOf(Tag.InstanceCreationTime), tagi.next());
		assertEquals(Integer.valueOf(Tag.SOPClassUID), tagi.next());
		// and on and on...
	}
}
