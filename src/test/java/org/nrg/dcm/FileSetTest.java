/**
 * Copyright (c) 2006-2012 Washington University
 */
package org.nrg.dcm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.dcm4che2.data.Tag;
import org.junit.Test;
import org.nrg.attr.ConversionFailureException;

import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;

public class FileSetTest {
    // Uses sample1 dataset
    final static private File dir = new File(System.getProperty("sample.data.dir"));
    final static private int numImageFiles = 528;
    final static private int numSequences = 2;
    final static private int numSeries = 3;
    final static private int numReferringPhys = 1;
    final static private String aSequenceName = "*tfl3d1_ns";
    final static private int numInASequence = 352;
    final static private int numSeriesInASequence = 2;
    final static private String oneSeriesInASequence = "4";
    final static private int numInOneSeriesInASequence = 176;
    final static private String modelName = "TrioTim";
    final static private String aSeriesNumber = "5";
    final static private int numInASequenceSeries = 176;

    private static FileSet dds;
    private final static File file1;
    private final static File file2;
    private final static File file3;

    static {
        try {
            dds = new FileSet(dir);
        } catch (IOException e) {
            fail("unable to load FileSet " + dir + " : " + e.getMessage());
            throw new RuntimeException("fail failed?");
        } catch (SQLException e) {
            fail("unable to load FileSet " + dir + " : " + e.getMessage());
            throw new RuntimeException("fail failed?");
        }
        if (dds == null)
            fail("unable to load FileSet " + dir);

        try {
            file1 = new File(dir, "1.MR.head_DHead.4.3.20061214.091206.156000.0506717986.dcm.gz").getCanonicalFile();
            file2 = new File(dir, "1.MR.head_DHead.4.11.20061214.091206.156000.8007818018.dcm.gz").getCanonicalFile();
            file3 = new File(dir, "1.MR.head_DHead.4.12.20061214.091206.156000.6027118028.dcm.gz").getCanonicalFile();
        } catch (IOException e) {
            fail("unable to get canonical test files: " + e.getMessage());
            throw new RuntimeException("fail failed?");
        }
    }

    @Test
    public final void testEquals() {
        assertTrue(dds.equals(dds));
        FileSet mydds = null;
        assertFalse(dds.equals(mydds));
        try {
            mydds = new FileSet(dir);
        } catch (IOException e) {
            fail(e.getMessage());
            return;
        } catch (SQLException e) {
            fail(e.getMessage());
            return;
        }
        assertTrue(dds.equals(mydds));
        assertTrue(mydds.equals(dds));
        mydds.dispose();
    }

    @Test
    public final void testFileSet() throws Exception {
        FileSet mydds = new FileSet(dir);
        assertNotNull(mydds);
        mydds.dispose();
    }

    static public class Patient extends DirectoryRecord {
        private static Set<Patient> all = new HashSet<Patient>();
        private static int count = 0;
        public Patient(final DirectoryRecord upper, final Map<Integer,String> vals) {
            super(DirectoryRecord.Template.PATIENT, upper, vals);
            if (all.add(this))
                count++;
        }
        public static int getCount() { return count; }
    }

    static public class Study extends DirectoryRecord {
        private static Set<Study> all = new HashSet<Study>();
        private static int count = 0;
        public Study(final DirectoryRecord upper, final Map<Integer,String> vals) {
            super(DirectoryRecord.Template.STUDY, upper, vals);
            if (all.add(this))
                count++;
        }
        public static int getCount() { return count; }
    }

    static public class Series extends DirectoryRecord {
        private static Set<Series> all = new HashSet<Series>();
        private static int count = 0;
        public Series(final DirectoryRecord upper, final Map<Integer,String> vals) {
            super(DirectoryRecord.Template.SERIES, upper, vals);
            if (all.add(this))
                count++;
        }
        public static int getCount() { return count; }
    }

    static public class Instance extends DirectoryRecord {
        private static Set<Instance> all = new HashSet<Instance>();
        private static int count = 0;
        public Instance(final DirectoryRecord upper, final Map<Integer,String> vals) {
            super(DirectoryRecord.Template.INSTANCE, upper, vals);
            if (all.add(this))
                count++;
        }
        public static int getCount() { return count; }
        public File getFile() { return new File(getValue(Tag.ReferencedFileID)); }
        @Override
        public String toString() {
            return super.toString() + " : " + getFile().getPath();
        }
    }


    @Test
    public final void testFileSetWithRecordConstructors() throws Exception {
        FileSet dds1 = new FileSet(dir, null);
        assertNotNull(dds1);
        assertNull(dds1.getPatientDirRecords());
        dds1.dispose();

        DirectoryRecord.Factory factory = new DirectoryRecord.Factory() {
            public DirectoryRecord newInstance(DirectoryRecord.Type type, DirectoryRecord upper, final Map<Integer,String> vals) {
                if (type == DirectoryRecord.Type.INSTANCE) return new Instance(upper, vals);
                if (type == DirectoryRecord.Type.SERIES) return new Series(upper, vals);
                if (type == DirectoryRecord.Type.STUDY) return new Study(upper, vals);
                if (type == DirectoryRecord.Type.PATIENT) return new Patient(upper, vals);
                throw new IllegalArgumentException("invalid DirectoryRecord type");
            }
            public Collection<Integer> getSelectionKeys(DirectoryRecord.Type type) {
                return DirectoryRecord.getDefaultFactory().getSelectionKeys(type);
            }
        };

        FileSet mydds = new FileSet(dir, new HashMap<String,String>(), factory);
        assertNotNull(mydds);

        assertEquals(1, Patient.getCount());
        assertEquals(1, Study.getCount());
        assertEquals(numSeries, Series.getCount());
        assertTrue(numImageFiles <= Instance.getCount()); // may be duplicates

        Set<DirectoryRecord> patients = mydds.getPatientDirRecords();
        assertEquals(1, patients.size());
        DirectoryRecord patient = patients.iterator().next();
        assertNotNull(patient);
        assertEquals(DirectoryRecord.Type.PATIENT, patient.getType());
        assertNull(patient.getUpper());
        assertEquals(1, patient.getLower().size());

        DirectoryRecord study = patient.getLower().iterator().next();
        assertEquals(DirectoryRecord.Type.STUDY, study.getType());
        assertTrue(study instanceof Study);
        assertEquals(patient, study.getUpper());

        DirectoryRecord series = study.getLower().iterator().next();
        assertEquals(DirectoryRecord.Type.SERIES, series.getType());
        assertTrue(series instanceof Series);
        assertEquals(study, series.getUpper());

        DirectoryRecord instancer = series.getLower().iterator().next();
        assertTrue(instancer instanceof Instance);
        Instance instance = (Instance) instancer;
        assertEquals(DirectoryRecord.Type.INSTANCE, instance.getType());
        assertEquals(0, instance.getLower().size());
        assertNotNull(instance.getFile());
        assertTrue(mydds.getDataFiles().contains(instance.getFile()));

        mydds.dispose();
    }


    @Test
    public final void testGetDataFiles() throws Exception {
        Set<File> files = dds.getDataFiles();
        assertTrue(numImageFiles <= files.size());  // may be duplicates
    }

    @Test
    public final void testGetFilesForValues() throws Exception {
        if (dds != null) dds.dispose();
        dds = new FileSet(dir);	// make sure we have clean cache
        assertTrue(numImageFiles <= dds.size());  // may be duplicates

        final Map<Integer,String> attrValues = new HashMap<Integer,String>();

        final Map<Integer,ConversionFailureException> failures = new HashMap<Integer,ConversionFailureException>();
        Set<File> files = dds.getFilesForValues(attrValues, failures);
        assertTrue(failures.isEmpty());
        assertTrue(numImageFiles <= files.size());    // may be duplicates

        attrValues.put(Tag.SequenceName, aSequenceName);
        files = dds.getFilesForValues(attrValues, failures);
        assertTrue(failures.isEmpty());
        assertTrue(numInASequence <= files.size());   // may be duplicates

        attrValues.put(Tag.ManufacturerModelName, modelName);
        files = dds.getFilesForValues(attrValues, failures);
        assertTrue(failures.isEmpty());
        assertTrue(numInASequence <= files.size());   // may be duplicates

        attrValues.put(Tag.SeriesNumber, aSeriesNumber);
        files = dds.getFilesForValues(attrValues, failures);
        assertTrue(failures.isEmpty());
        assertTrue(numInASequenceSeries <= files.size()); // may be duplicates

        // Now make sure that it works with cached values
        dds.getUniqueValues(Tag.SequenceName);
        dds.getUniqueValues(Tag.SeriesNumber);
        files = dds.getFilesForValues(attrValues, failures);
        assertTrue(failures.isEmpty());
        assertTrue(numInASequenceSeries <= files.size());
    }

    @Test
    public final void testGetUniqueCombinations() throws Exception  {
        final Map<Integer,ConversionFailureException> failures = new HashMap<Integer,ConversionFailureException>();
        final Set<Integer> tags = new HashSet<Integer>();
        Set<Map<Integer,String>> combs = new HashSet<Map<Integer,String>>();
        combs = dds.getUniqueCombinations(tags, failures);
        assertTrue(failures.isEmpty());

        tags.add(Tag.SequenceName);
        combs = dds.getUniqueCombinations(tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(numSequences, combs.size());

        tags.add(Tag.SeriesNumber);
        combs = dds.getUniqueCombinations(tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(numSeries, combs.size());

        tags.add(Tag.OperatorsName);
        try {
            combs = dds.getUniqueCombinations(tags, failures);
            assertTrue(failures.isEmpty());
        } catch (IOException e) {
            fail(e.getMessage());
        }
        assertEquals(numSeries, combs.size());
    }

    @Test
    public final void testGetUniqueCombinationsGivenValues() throws Exception {
        final Map<Integer,ConversionFailureException> failures = new HashMap<Integer,ConversionFailureException>();
        final Map<Integer,String> given = new HashMap<Integer,String>();
        final Set<Integer> tags = new HashSet<Integer>();
        Set<Map<Integer,String>> combs = new HashSet<Map<Integer,String>>();

        // empty given and tags
        combs = dds.getUniqueCombinationsGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());

        // empty given, nonempty tags
        tags.add(Tag.SequenceName);
        combs = dds.getUniqueCombinationsGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(numSequences, combs.size());

        // Adding up the number of files per value over all values should
        // give us the total file count.
        int filesCount = 0;
        for (Map<Integer,String> comb : combs) {
            assertEquals(1, comb.size());	// we only asked for one value
            filesCount += dds.getFilesForValues(comb, failures).size();
            assertTrue(failures.isEmpty());
        }
        assertTrue(numImageFiles <= filesCount);    // may be duplicates

        // one given, one tag
        given.clear();
        given.put(Tag.SequenceName, aSequenceName);
        tags.clear();
        tags.add(Tag.SeriesNumber);

        combs = dds.getUniqueCombinationsGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(numSeriesInASequence, combs.size());

        // multiple given, one tag
        given.put(Tag.SeriesNumber, oneSeriesInASequence);
        tags.clear();
        tags.add(Tag.InstanceNumber);

        // this should give us one combination, (sequence series instance),
        // for each file in the sequence.
        combs = dds.getUniqueCombinationsGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(numInOneSeriesInASequence, combs.size());

        // multiple given, multiple tags
        tags.add(Tag.AcquisitionTime);

        // this should give us one combination, (sequence series instance time),
        // for each file in the sequence.
        combs = dds.getUniqueCombinationsGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(numInOneSeriesInASequence, combs.size());
    }

    @Test
    public final void testGetUniqueValuesFromFiles() throws Exception {
        final Map<Integer,ConversionFailureException> failures = new HashMap<Integer,ConversionFailureException>();
        final Set<File> files = new HashSet<File>();
        final Set<Integer> tags = new HashSet<Integer>();
        SetMultimap<Integer,String> result;

        result = dds.getUniqueValuesFromFiles(files, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(0, result.size());

        files.add(file1);
        result = dds.getUniqueValuesFromFiles(files, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(0, result.size());

        tags.add(Tag.InstanceNumber);
        tags.add(Tag.SOPInstanceUID);
        result = dds.getUniqueValuesFromFiles(files, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(2, result.size());
        for (int tag : result.keySet())
            assertEquals(1, result.get(tag).size());

        files.add(file2);
        files.add(file3);
        result = dds.getUniqueValuesFromFiles(files, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(2, result.keySet().size());
        for (int tag : result.keySet())
            assertEquals(3, result.get(tag).size());

        tags.clear();
        tags.add(Tag.SeriesInstanceUID);
        tags.add(Tag.SeriesNumber);
        tags.add(Tag.PatientName);
        result = dds.getUniqueValuesFromFiles(files, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(3, result.size());
        for (int tag : result.keySet()) {
            assertEquals(1, result.get(tag).size());
        }
    }


    @Test
    public final void testGetUniqueValuesGivenValues() throws Exception {
        final Map<Integer,ConversionFailureException> failures = new HashMap<Integer,ConversionFailureException>();
        final Map<Integer,String> given = new HashMap<Integer,String>();
        final Set<Integer> tags = new HashSet<Integer>();
        SetMultimap<Integer,String> result;

        result = dds.getUniqueValuesGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(0, result.size());

        tags.add(Tag.SequenceName);
        result = dds.getUniqueValuesGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(1, result.keySet().size());
        assertEquals(numSequences, result.get(Tag.SequenceName).size());

        given.put(Tag.SequenceName, aSequenceName);
        tags.add(Tag.SeriesNumber);
        result = dds.getUniqueValuesGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(2, result.keySet().size());
        assertEquals(1, result.get(Tag.SequenceName).size());
        assertEquals(numSeriesInASequence, result.get(Tag.SeriesNumber).size());

        given.put(Tag.SeriesNumber, aSeriesNumber);
        tags.add(Tag.ImagePositionPatient);
        result = dds.getUniqueValuesGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(3, result.keySet().size());
        assertEquals(numInASequenceSeries, result.get(Tag.ImagePositionPatient).size());
    }

    @Test
    public final void testGetUniqueValuesInt() throws Exception {
        Set<String> values = dds.getUniqueValues(Tag.SequenceName);
        assertNotNull(values);
        assertEquals(numSequences, values.size());
    }

    @Test
    public final void testGetUniqueValuesSetOfInteger() throws Exception {
        final Map<Integer,ConversionFailureException> failures = new HashMap<Integer,ConversionFailureException>();
        final Set<Integer> tags = new HashSet<Integer>();
        tags.add(Tag.SOPInstanceUID);
        tags.add(Tag.ReferringPhysicianName);
        tags.add(Tag.SeriesNumber);
        final Multimap<Integer,String> values = dds.getUniqueValues(tags, failures);
        assertTrue(failures.isEmpty());

        assertNotNull(values);
        assertNotNull(values.get(Tag.SOPInstanceUID));
        assertEquals(numImageFiles, values.get(Tag.SOPInstanceUID).size());
        assertNotNull(values.get(Tag.ReferringPhysicianName));
        assertEquals(numReferringPhys, values.get(Tag.ReferringPhysicianName).size());
        assertNotNull(values.get(Tag.SeriesNumber));
        assertEquals(numSeries, values.get(Tag.SeriesNumber).size());

        // didn't ask for this info
        assertFalse(values.containsKey(Tag.ImageType));
    }

    @Test
    public final void testSize() throws Exception {
        final FileSet fs = new FileSet(new File[]{});
        try {
            assertEquals(0, fs.size());
            fs.add(new File[]{file1});
            assertEquals(1, fs.size());
            fs.add(new File[]{file2, file3});
            assertEquals(3, fs.size());
            fs.remove(new File[]{file2});
            assertEquals(2, fs.size());
            fs.remove(new File[]{file1, file3});
            assertEquals(0, fs.size());
        } finally {
            fs.dispose();
        }
    }
    
    @Test
    public final void testIsEmpty() throws Exception {
        final FileSet fs = new FileSet(new File[]{});
        try {
            assertTrue(fs.isEmpty());
            fs.add(new File[]{file1});
            assertFalse(fs.isEmpty());
            fs.add(new File[]{file2});
            assertFalse(fs.isEmpty());
            fs.remove(new File[]{file1, file2});
            assertTrue(fs.isEmpty());
        } finally {
            fs.dispose();
        }
    }
}
