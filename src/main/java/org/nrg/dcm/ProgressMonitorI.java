/**
 * $Id: ProgressMonitorI.java,v 1.1 2006/12/21 22:00:26 karchie Exp $
 * Copyright (c) 2006 Washington University
 */
package org.nrg.dcm;

/**
 * Interface for progress notification (e.g., for progress bars)
 * This is partially compatible with the Swing ProgressMonitor,
 * though it leaves out a few methods.
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 */
public interface ProgressMonitorI {
  public void setMinimum(int min);
  public void setMaximum(int max);
  public void setProgress(int current);
  public void setNote(String note);
  public void close();
  public boolean isCanceled();
}
