/**
 * $Id: AttrDefSet.java,v 1.2 2007/08/08 20:04:18 karchie Exp $
 */
package org.nrg.dcm;

/**
 * Describes a group of external attributes and their conversions from DICOM fields
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class AttrDefSet extends org.nrg.attr.AttrDefSet<Integer,String> {}
