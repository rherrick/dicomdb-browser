/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.dcm;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 */
public abstract class DicomException extends Exception {
  private static final long serialVersionUID = 1L;
  
  public DicomException(String arg0) {
    super(arg0);
  }

  public DicomException(Throwable arg0) {
    super(arg0);
  }

  public DicomException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

}
